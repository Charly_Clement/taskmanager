<?php

    class Tache {

        public $_nom;
        public $_id;

        public function __construct($nom, $id) {

            $this->_nom = $nom;
            $this->_id = $id;
        }

        public function __toString() {

            return $this->_nom;
        }

    // GETTERS
        public function getNom() {
            return $this->_nom;
        }
        public function getId() {
            return $this->_id;
        }

    // SETTERS
        public function setNom() {
            return $this->_nom;
        }
        public function setId() {
            return $this->_id;
        }


    }

?>